module P00(
input wire [17:0] SW,
output reg [17:0] LEDR,
output reg [7:0] HEX0, HEX1, HEX2, HEX3, HEX4, HEX5, HEX6, HEX7
);

wire [3:0] Digit[7:0];
reg [15:0] result;

always @(SW[17:2]) begin
  reg [15:0] A, B;
  reg [1:0] op;
  A = SW[17:11];
  B = SW[10:4];
  op = SW[3:2];
  if (op == 0)
    result = A + B;
  else if (op == 1)
    result = A - B;
  else if (op == 2)
    result = A * B;
  else
    result = (A / B * 100) + A % B;
end

always begin
LEDR[11] = 1'b1;
LEDR[4] = 1'b1;
end

binary_to_BCD(SW[17:11], Digit[6], Digit[7]);
binary_to_BCD(SW[10:4], Digit[4], Digit[5]);
binary_to_BCD(result, Digit[0], Digit[1], Digit[2], Digit[3]);

//Print Digit to 7-Segement
Seg7 h0( Digit[0], HEX0 );
Seg7 h1( Digit[1], HEX1 );
Seg7 h2( Digit[2], HEX2 );
Seg7 h3( Digit[3], HEX3 );
Seg7 h4( Digit[4], HEX4 );
Seg7 h5( Digit[5], HEX5 );
Seg7 h6( Digit[6], HEX6 );
Seg7 h7( Digit[7], HEX7 );

endmodule

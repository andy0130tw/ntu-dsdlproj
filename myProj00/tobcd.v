module binary_to_BCD(A,ONES,TENS,HUNDREDS,THOUSAND,TEN_THOU);
 input [15:0] A;
 output reg [3:0] ONES, TENS;
 output reg [3:0] HUNDREDS,THOUSAND,TEN_THOU;
 
 integer i;
 always@(A)
 begin 
    reg [15:0] tmp;
    reg op;
	 op = 0;
	 if (A[15]) begin
	   op = 1;
		tmp = ~A + 1;
    end else 
	   tmp = A;
	 
    TEN_THOU=4'd0;
    THOUSAND=4'd0;
    HUNDREDS=4'd0;
    TENS=4'd0;
    ONES=4'd0;
    
    for(i=15;i>=0;i=i-1)
    begin  
      //Add 3 to columns >=5
      if (TEN_THOU>=5)
          TEN_THOU=TEN_THOU+3;
      if (THOUSAND >=5)
        THOUSAND=THOUSAND+3;
      if (HUNDREDS >=5)
         HUNDREDS=HUNDREDS+3;
      if (TENS >=5)
        TENS=TENS+3;
      if (ONES >=5)
         ONES=ONES+3;
            
      //Shift left one
      TEN_THOU=TEN_THOU<<1;
      TEN_THOU[0]=THOUSAND[3];
      
      THOUSAND=THOUSAND<<1;
      THOUSAND[0]=HUNDREDS[3];
      
      HUNDREDS=HUNDREDS<<1;
      HUNDREDS[0]=TENS[3];
      
      TENS=TENS<<1;
      TENS[0]=ONES[3];
      
      ONES=ONES<<1;
      ONES[0]=tmp[i];
 end
 
 if (op)
    THOUSAND = 4'd10;
end 

endmodule


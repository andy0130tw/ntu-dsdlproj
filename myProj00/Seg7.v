/* 七段顯示器解碼(共陽) */
module Seg7( DIG_In, DIG_Out );

    input  [3:0] DIG_In;    // MSB D, C, B, A LSB
    output reg [7:0] DIG_Out;   // MSB dp, g, f, e, d, c, b, a LSB

    always @( DIG_In ) begin
        case( DIG_In )
            4'b0000:    DIG_Out <= 8'b11000000;    // 0
            4'b0001:    DIG_Out <= 8'b11111001;    // 1
            4'b0010:    DIG_Out <= 8'b10100100;    // 2
            4'b0011:    DIG_Out <= 8'b10110000;    // 3
            4'b0100:    DIG_Out <= 8'b10011001;    // 4
            4'b0101:    DIG_Out <= 8'b10010010;    // 5
            4'b0110:    DIG_Out <= 8'b10000011;    // 6
            4'b0111:    DIG_Out <= 8'b11111000;    // 7
            4'b1000:    DIG_Out <= 8'b10000000;    // 8
            4'b1001:    DIG_Out <= 8'b10010000;    // 9
            4'b1010:    DIG_Out <= 8'b10111111;    // minus
            default:    DIG_Out <= 8'b11111111;    // off
        endcase
    end

endmodule
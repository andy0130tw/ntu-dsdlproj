module D9487(
input wire On,
output wire [3:0] Digit[3:0]
);
	
	always @(On) begin
		if(On) begin
			Digit[0] = 4'd7;
			Digit[1] = 4'd8;
			Digit[2] = 4'd4;
			Digit[3] = 4'd9;
		end
		else begin
			Digit[0] = 4'd9;
			Digit[1] = 4'd6;
			Digit[2] = 4'd2;
			Digit[3] = 4'd5;
		end
	end

endmodule
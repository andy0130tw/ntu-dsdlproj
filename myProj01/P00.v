module P00(
  input  [17:0] SW,
  input  [3:0] KEY,
  input  CLOCK_50,  // 50MHz clock
  output reg [6:0] HEX0, HEX1, HEX2, HEX3, HEX4, HEX5, HEX6, HEX7,
  
  // LCD Module 16x2
  
  inout [7:0] LCD_DATA, // LCD Data bus 8 bits
  output LCD_ON,        // LCD Power ON/OFF
  output LCD_BLON,      // LCD Back Light ON/OFF
  output LCD_RW,        // LCD Read/Write Select, 0 = Write, 1 = Read
  output LCD_EN,        // LCD Enable
  output LCD_RS         // LCD Command/Data Select, 0 = Command, 1 = Data
);

wire [3:0] Digit[7:0];
wire [23:0] OUT;
wire CLK_Out;

Freq_Divider FreqDiv(CLOCK_50, KEY[0], CLK_Out);

wire [23:0] OUT1, OUT2;

counter ctr(CLK_Out, KEY, SW[3:0], OUT1);
counterClock ctrClock(CLK_Out, KEY, SW[17:0], OUT2);

num_split oDigit(OUT, {
    Digit[7], Digit[6], Digit[5], Digit[4],
    Digit[3], Digit[2], Digit[1], Digit[0]
});

always @(SW[0]) begin
  if (SW[0]) begin
    OUT = OUT2;
  end else begin
    OUT = OUT1;
  end
end

//Print Digit to 7-Segement
Seg7 h0( Digit[0], HEX0 );
Seg7 h1( Digit[1], HEX1 );
Seg7 h2( Digit[2], HEX2 );
Seg7 h3( Digit[3], HEX3 );
Seg7 h4( Digit[4], HEX4 );
Seg7 h5( Digit[5], HEX5 );
Seg7 h6( Digit[6], HEX6 );
Seg7 h7( Digit[7], HEX7 );

reg [31:0] time1 = 32'b0, time2 = 32'b0;
reg lcdtoggle = 1'b0;

always @(negedge SW[16]) begin
  // only in stopwatch
  reg [63:0] state;
  state[31:0] = { Digit[7], Digit[6], Digit[5], Digit[4],
                  Digit[3], Digit[2], Digit[1], Digit[0] };
  if (SW[17]) begin
    time1 <= 32'b0;
    time2 <= 32'b0;
  end else begin
    state[63:0] = {time2, Digit[7], Digit[6], Digit[5], Digit[4],
                          Digit[3], Digit[2], Digit[1], Digit[0] };
	 time1[31:0] <= state[63:32];
  	 time2[31:0] <= state[31:0];
  end
  //lcdtoggle = !lcdtoggle;
end

// %%%%%%%%%% LCD section %%%%%%%%%%

wire [6:0] myclock;
clock_divider cdiv(CLOCK_50, 1, myclock);

reg timer_state;

// set up counters

wire pulse;

oneshot pulser1(
    .pulse_out(pulse),
    .trigger_in(timer_state),
    .clk(CLOCK_50)
);

wire lcd_start;
oneshot pulser2(
    .pulse_out(lcd_start),
    .trigger_in(myclock[0]),
    .clk(CLOCK_50)
);

// reset delay gives some time for peripherals to initialize
wire DLY_RST;
Reset_Delay r0( .iCLK(CLOCK_50),.oRESET(DLY_RST) );

/*
always @ (negedge KEY[3] or negedge RST)
begin
    if (!RST) timer_state <= 1'b0;
    else timer_state <= ~timer_state;
end
*/

//  Internal Wires/Registers
reg [5:0]   LUT_INDEX;
reg [8:0]   LUT_DATA;
reg [5:0]   mLCD_ST;
reg [17:0]  mDLY;
reg         mLCD_Start;
reg [7:0]   mLCD_DATA;
reg         mLCD_RS;
wire        mLCD_Done;

parameter    LCD_INTIAL   =    0;
parameter    LCD_LINE1    =    5;
parameter    LCD_CH_LINE  =    LCD_LINE1+16;
parameter    LCD_LINE2    =    LCD_LINE1+16+1;
parameter    LUT_SIZE     =    LCD_LINE1+32+1;

always
begin
    case(LUT_INDEX)
    LCD_INTIAL+0:   LUT_DATA    <=  9'h038;
    LCD_INTIAL+1:   LUT_DATA    <=  9'h00C;
    LCD_INTIAL+2:   LUT_DATA    <=  9'h001;
    LCD_INTIAL+3:   LUT_DATA    <=  9'h006;
    LCD_INTIAL+4:   LUT_DATA    <=  9'h080;
    //  Line 1
    LCD_LINE1+0:    LUT_DATA    <=  9'h130 | time1[31:28];
    LCD_LINE1+1:    LUT_DATA    <=  9'h130 | time1[27:24];
    LCD_LINE1+2:    LUT_DATA    <=  9'h13A; // :
    LCD_LINE1+3:    LUT_DATA    <=  9'h130 | time1[23:20];
    LCD_LINE1+4:    LUT_DATA    <=  9'h130 | time1[19:16];
    LCD_LINE1+5:    LUT_DATA    <=  9'h13A; // :
    LCD_LINE1+6:    LUT_DATA    <=  9'h130 | time1[15:12];
    LCD_LINE1+7:    LUT_DATA    <=  9'h130 | time1[11:8];
    LCD_LINE1+8:    LUT_DATA    <=  9'h12E; // .
    LCD_LINE1+9:    LUT_DATA    <=  9'h130 | time1[7:4];
    LCD_LINE1+10:   LUT_DATA    <=  9'h130 | time1[3:0];
    LCD_LINE1+11:   LUT_DATA    <=  9'h120;
    LCD_LINE1+12:   LUT_DATA    <=  9'h120;
    LCD_LINE1+13:   LUT_DATA    <=  9'h120;
    LCD_LINE1+14:   LUT_DATA    <=  9'h120;
    LCD_LINE1+15:   LUT_DATA    <=  9'h120;
    // Change line
    LCD_CH_LINE:    LUT_DATA    <=  9'h0C0;
    //    Line 2
    LCD_LINE2+0:    LUT_DATA    <=  9'h130 | time2[31:28];
    LCD_LINE2+1:    LUT_DATA    <=  9'h130 | time2[27:24];
    LCD_LINE2+2:    LUT_DATA    <=  9'h13A; // :
    LCD_LINE2+3:    LUT_DATA    <=  9'h130 | time2[23:20];
    LCD_LINE2+4:    LUT_DATA    <=  9'h130 | time2[19:16];
    LCD_LINE2+5:    LUT_DATA    <=  9'h13A; // :
    LCD_LINE2+6:    LUT_DATA    <=  9'h130 | time2[15:12];
    LCD_LINE2+7:    LUT_DATA    <=  9'h130 | time2[11:8];
    LCD_LINE2+8:    LUT_DATA    <=  9'h12E; // .
    LCD_LINE2+9:    LUT_DATA    <=  9'h130 | time2[7:4];
    LCD_LINE2+10:   LUT_DATA    <=  9'h130 | time2[3:0];
    LCD_LINE2+11:   LUT_DATA    <=  9'h120;
    LCD_LINE2+12:   LUT_DATA    <=  9'h120;
    LCD_LINE2+13:   LUT_DATA    <=  9'h120;
    LCD_LINE2+14:   LUT_DATA    <=  9'h120;
    LCD_LINE2+15:   LUT_DATA    <=  9'h120;
    default:        LUT_DATA    <=  9'dx;
    endcase
end

// turn LCD ON
assign  LCD_ON      =   1'b1;
assign  LCD_BLON    =   1'b1;


always@(posedge CLOCK_50 or negedge DLY_RST or posedge lcd_start)
begin
    if(!DLY_RST || lcd_start)
    begin
        LUT_INDEX   <=  0;
        mLCD_ST     <=  0;
        mDLY        <=  0;
        mLCD_Start  <=  0;
        mLCD_DATA   <=  0;
        mLCD_RS     <=  0;
    end
    else
        begin
        case(mLCD_ST)
        0:  begin
                mLCD_DATA   <=  LUT_DATA[7:0];
                mLCD_RS     <=  LUT_DATA[8];
                mLCD_Start  <=  1;
                mLCD_ST     <=  1;
            end
        1:  begin
                if(mLCD_Done)
                begin
                    mLCD_Start  <=  0;
                    mLCD_ST     <=  2;                  
                end
            end
        2:  begin
                if(mDLY<18'h3FFFE)
                    mDLY    <=  mDLY + 1'b1;
                else
                begin
                    mDLY    <=  0;
                    mLCD_ST <=  3;
                end
            end
        3:  begin               
                if (LUT_INDEX<LUT_SIZE) begin
                    mLCD_ST <= 0;
                    LUT_INDEX <= LUT_INDEX + 1'b1;
                end
            end
        endcase
    end
end


LCD_Controller u0(
    //    Host Side
    .iDATA(mLCD_DATA),
    .iRS(mLCD_RS),
    .iStart(mLCD_Start),
    .oDone(mLCD_Done),
    .iCLK(CLOCK_50),
    .iRST_N(DLY_RST),
    //    LCD Interface
    .LCD_DATA(LCD_DATA),
    .LCD_RW(LCD_RW),
    .LCD_EN(LCD_EN),
    .LCD_RS(LCD_RS)
);

endmodule

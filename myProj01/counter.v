module counter(input clk, input[3:0] keys, input[3:0] sw, output reg[23:0] CLK_Cnt);
	reg isPaused = 1'b1;
	reg isReset = 1'b0;
	reg isClear = 1'b0;
	reg stepBackward = 1'b0;
	
	always @(negedge keys[0]) begin
		isPaused <= !isPaused;
	end
	
	always @(keys[1]) begin
	   isClear <= !keys[1];
	end
	
	always @(negedge keys[1]) begin
		if (!isPaused && !isReset) begin
		  isReset <= 1'b1;
		end else if (isPaused && isReset) begin
		  isReset <= 1'b0;
		end
	end
	
	always @(sw[1]) begin
		stepBackward<= sw[1];
	end

	always @(posedge clk) begin
		if (isClear) begin
			CLK_Cnt = 0;
		end else if (isReset == isPaused) begin
			CLK_Cnt = CLK_Cnt + (stepBackward ? -1 : 1);
		end
		
	end
endmodule

module num_split(input [23:0] iNum, output reg[31:0] oNum);
	wire [5:0] sec;
	wire [5:0] min;
	wire [5:0] hr;

	always @(iNum) begin
		sec = iNum / 100 % 60;
		min = iNum / 6000 % 60;
		hr = iNum / 360000;
		
   	oNum[3:0] = iNum % 10;
		oNum[7:4] = iNum / 10 % 10;
		oNum[11:8] = sec % 10;
		oNum[15:12] = sec / 10;
		// minutes
		oNum[19:16] = min % 10;
		oNum[23:20] = min / 10;
		// hours
		oNum[27:24] = hr % 10;
		oNum[31:28] = hr / 10;
	end
endmodule

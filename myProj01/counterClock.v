module counterClock(input clk, input[3:0] keys, input[17:0] sw, output reg[23:0] CLK_Cnt);
	reg isPaused = 1'b1;
	reg isReset = 1'b0;
	reg isSetTime = 1'b0;
	reg isClear = 1'b0;
	reg stepBackward = 1'b0;
	
	always @(negedge keys[0]) begin
		if (!isSetTime) begin
		  isPaused <= !isPaused;
		end
	end
	
	always @(keys[1]) begin
	   isClear = !keys[1];
	end
	
	always @(negedge keys[1]) begin
		if (!isPaused && !isReset) begin
		  isReset <= 1'b1;
		end else if (isPaused && isReset) begin
		  isReset <= 1'b0;
		end
	end
	
	always @(sw[1]) begin
		stepBackward<= sw[1];
	end
	
	always @(sw[2]) begin
	   isSetTime <= sw[2];
	end
	
	always @(posedge clk) begin
	   reg [2:0] hr;
		reg [5:0] min, sec;
		if (isClear) begin
			CLK_Cnt = 0;
		end else if (isSetTime) begin
		   // 17:15 hr
			// 14: 9 min
			//  8: 3 sec
			hr = sw[17:15];
			min = sw[14:9];
			sec = sw[8:3];
			CLK_Cnt = (hr * 3600 + (min > 59 ? 59 : min) * 60 + (sec > 59 ? 59 : sec)) * 100;
		end else if (isReset == isPaused) begin
			CLK_Cnt = CLK_Cnt + (stepBackward ? -1 : 1);
		end
	end
endmodule
